package kucingxburung;
 import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Kucingxburung {
   public static void main(String[] args) {
      String regex = "kucing";
      String input = "kucing itu bercuit, semua kucing bercuit.";
      String replace = "burung";
      
      // Compile regex pattern
      Pattern pattern = Pattern.compile(regex);
      
      // Create matcher object
      Matcher matcher = pattern.matcher(input);
        
      // Replace all occurrences of regex with replace string
      String output = matcher.replaceAll(replace);
      
      System.out.println(output);
   }
}
