/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package exam2;
import java.util.HashMap;
import java.util.Map;
public class Exam2{
    public static int findSingleNumber(int[] arr) {
        Map<Integer, Integer> counts = new HashMap<>();
        for (int num : arr) {
            counts.put(num, counts.getOrDefault(num, 0) + 1);
        }
        for (int num : arr) {
            if (counts.get(num) == 1) {
                return num;
            }
        }
        return -1;
    }
    public static int findPosition(int[] arr, int target) {  
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == target) {
                return i + 1; 
            }
        }
        return -1; 
    }
    public static void main(String[] args) {
        int[] arr = {102, 32, 99, 32, 45, 102, 45, 67, 67, 100, 100};
        int singleNumber = findSingleNumber(arr);
        int position = findPosition(arr, singleNumber);
        System.out.println("Bilangan yang tidak berpasangan adalah " + singleNumber + " pada posisi ke-" + position + ".");
    }
}

