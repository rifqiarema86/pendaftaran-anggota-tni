package keywordsearch;
public class Keywordsearch {

    public static void main(String[] args) {
        String kataKunci = "Hati";
        String kalimat1 = "Hatiku sungguh senang sekali";
        String kalimat2 = "Pukulan yang sangat keras";
        String kalimat3 = "Kalau berjalan hati – hati";

        System.out.println("pada kalimat pertama : " + cekKata(kataKunci, kalimat1));
        System.out.println("pada kalimat kedua : " + cekKata(kataKunci, kalimat2));
        System.out.println("pada kalimat ketiga : " + cekKata(kataKunci, kalimat3));
    }

    public static boolean cekKata(String kataKunci, String kalimat) {
        return kalimat.toLowerCase().contains(kataKunci.toLowerCase());
    }
}