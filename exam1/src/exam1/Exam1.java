/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package exam1;
public class Exam1 {
    public static void main(String[] args) {
        for (int i = 1; i <= 100; i++) {
            if (i % 3 == 0 && i % 5 == 0) {
                System.out.print("snip-snap, ");
            } else if (i % 3 == 0) {
                System.out.print("snip, ");
            } else if (i % 5 == 0) {
                System.out.print("snap, ");
            } else {
                System.out.print(i + ", ");
            }
        }
    }
}
