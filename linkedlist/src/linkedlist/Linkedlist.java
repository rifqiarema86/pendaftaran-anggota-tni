package linkedlist;
public class Linkedlist {
    Node head;  // node pertama dalam linked list
    
    // kelas Node
    static class Node {
        int data;
        Node next;

        Node(int d) {
            data = d;
            next = null;
        }
    }

    // metode untuk menambahkan node baru pada akhir linked list
   public void insert(int data) {
    Node new_node = new Node(data);

    // jika linked list masih kosong
    if (head == null) {
        head = new_node;
        return;
    }

    // jika nilai node baru lebih kecil dari nilai node pertama, maka node baru akan menjadi node pertama
    if (new_node.data < head.data) {
        new_node.next = head;
        head = new_node;
        return;
    }

    Node current = head;

    // mencari posisi yang tepat untuk node baru
    while (current.next != null && current.next.data < new_node.data) {
        current = current.next;
    }

    new_node.next = current.next;
    current.next = new_node;
}

    // metode untuk menampilkan isi linked list
    public void display() {
        Node current = head;
        while (current != null) {
            System.out.print(current.data + " ");
            current = current.next;
        }
        System.out.println();
    }
}