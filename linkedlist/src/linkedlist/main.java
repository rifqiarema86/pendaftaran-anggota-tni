package linkedlist; 
import java.util.LinkedList;
public class main {
    public static void main(String[] args) {
        Linkedlist list = new Linkedlist();

        // menambahkan node baru pada akhir linked list
        list.insert(30);
        list.insert(50);
        list.insert(10); 
        list.display();// Insert 2 items
        
        list.insert(90);
        list.insert(40);

        // menampilkan isi linked list
        list.display(); // OUTPUT : 10 30 40 50 90
    }
}
