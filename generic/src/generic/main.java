package generic;
public class main {
    public static void main(String[] args) {
        genericarray<Integer, String> carArray = new genericarray<>();
        carArray.insertItem(2010, "Ertiga");
        carArray.insertItem(2016, "Xpander");
        carArray.insertItem(2019, "Mobilio");

        System.out.println(carArray.printList());
        System.out.println("Total items: " + carArray.getTotalItem());
    }
}
