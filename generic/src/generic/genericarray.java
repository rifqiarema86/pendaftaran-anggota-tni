package generic;
import java.util.List;
import java.util.ArrayList;
public class genericarray<K, V> {
    private List<Generic<K, V>> items;
    
    public genericarray() {
        items = new ArrayList<>();
    }
    public String printList() {
        StringBuilder sb = new StringBuilder();
        for (Generic<K, V> item : items) {
            sb.append(item.getKey()).append(" - ").append(item.getValue()).append("\n");
        }
        return sb.toString();
    }

    public int getTotalItem() {
        return items.size();
    }

    public void insertItem(K key, V value) {
        Generic<K, V> item = new Generic<>(key, value);
        items.add(item);
    }
}