package Auth;

import java.util.HashMap;

public class Auth {
    private HashMap<String, String> tabelAkun;
    private HashMap<String, Boolean> tabelSesiLogin;

    public Auth() {
        this.tabelAkun = new HashMap<String, String>();
        this.tabelSesiLogin = new HashMap<String, Boolean>();
    }

    public Boolean registerAkun(String email, String password) {
        if (this.tabelAkun.containsKey(email)) {
            return false;
        }
        this.tabelAkun.put(email, password);
        return true;
    }

    public Boolean hapusAkun(String email, String konfirmasiPassword) {
        if (!this.tabelAkun.containsKey(email)) {
            return false;
        }
        String password = this.tabelAkun.get(email);
        if (!password.equals(konfirmasiPassword)) {
            return false;
        }
        this.tabelAkun.remove(email);
        return true;
    }

    public int totalEmailUMM() {
        int count = 0;
        for (String email : this.tabelAkun.keySet()) {
            if (email.endsWith("@umm.ac.id")) {
                count++;
            }
        }
        return count;
    }

    public static void main(String[] args) {
        Auth auth = new Auth();
        auth.registerAkun("labit@umm.ac.id", "Labit321");
        auth.registerAkun("kharismamuzaki@gmail.com", "Testing123");
        auth.registerAkun("byasageng@gmail.com", "cobaLagi321");
        auth.registerAkun("dosen.tersayang@umm.ac.id", "dosenkuGG");
        auth.registerAkun("email.saya@umm.ac.id", "email_Student");

        System.out.println("Total email UMM: " + auth.totalEmailUMM());

        System.out.println("Hapus akun email.saya@umm.ac.id");
        Boolean berhasilHapus = auth.hapusAkun("email.saya@umm.ac.id", "email_Student");
        if (berhasilHapus) {
            System.out.println("Akun berhasil dihapus");
        } else {
            System.out.println("Akun gagal dihapus");
        }
    }
}
