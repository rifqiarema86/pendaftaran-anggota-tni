package binnary;
import java.util.*;

public class Binnary {
    public static void main(String[] args) {
        // input array of numbers
        Double[] numbers = {2.3, 2.0, 1034923.0, 1.0};
        // input target number
        Scanner sc = new Scanner(System.in);
        System.out.print("Masukkan angka yang ingin dicari: ");
        double target = sc.nextDouble();
        // sort the array of numbers
        Arrays.sort(numbers);
        // perform binary search
        int result = Arrays.binarySearch(numbers, target);
        // output the result
        if (result >= 0) {
            System.out.println("Angka " + target + " ditemukan pada index ke " + result);
        } else {
            System.out.println("Angka " + target + " tidak ditemukan dalam array");
        }
    }
}
