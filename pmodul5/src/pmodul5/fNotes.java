package pmodul5;
import javax.swing.*; 
import java.io.*; 
import javax.swing.filechooser.FileFilter;
/**
 *
 * @author Lenovo
 */
public class fNotes extends javax.swing.JFrame {
    JFileChooser fc; 
 File file; 
 boolean fileBaru; 
 final String[] EXT = { ".csv" }; 
 final String[] EXT2 = { ".txt", ".java" };
    
    
    public fNotes() {
        
        initComponents();
        this.setTitle("Untitled - NOTES"); 
 //Atur Fillter jenis file dengan Handle fc 
 fc = new JFileChooser(); 
 fc.addChoosableFileFilter(new jenisFile(EXT)); 
 fc.addChoosableFileFilter(new jenisFile(EXT2)); 
 //Filter Bawaan (All Files) di disable-kan 
 fc.setAcceptAllFileFilterUsed(false);
 //variabel fileBaru diinisialisai dengan TRUE 
 fileBaru = true; 

    }
    
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnNew = new javax.swing.JButton();
        btnOpen = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        btnSaveAs = new javax.swing.JButton();
        btnKeluar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        editor = new javax.swing.JEditorPane();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        btnNew.setText("New");
        btnNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewActionPerformed(evt);
            }
        });

        btnOpen.setText("Open");
        btnOpen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOpenActionPerformed(evt);
            }
        });

        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        btnSaveAs.setText("Save As");
        btnSaveAs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveAsActionPerformed(evt);
            }
        });

        btnKeluar.setText("Keluar");
        btnKeluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnKeluarActionPerformed(evt);
            }
        });

        jScrollPane1.setViewportView(editor);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnNew)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnOpen)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSave)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSaveAs)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnKeluar)))
                .addContainerGap(10, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnNew)
                    .addComponent(btnOpen)
                    .addComponent(btnSave)
                    .addComponent(btnSaveAs)
                    .addComponent(btnKeluar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 224, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(42, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnNewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNewActionPerformed
// TODO add your handling code here: 
 //Jika Klik Baru maka 
 //editor dikosongkan, focus diarahkan ke editor 
 //Judul diganti menjadi Untitled,fileBaru menjadi TRUE 
 editor.setText(""); 
 editor.requestFocus(); 
 this.setTitle("Untitled - NOTES"); 
 fileBaru = true; 
    }//GEN-LAST:event_btnNewActionPerformed

    private void btnSaveAsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveAsActionPerformed
         if (fc.showSaveDialog(this) == JFileChooser.APPROVE_OPTION){ 
 //Pilih yes 
 file = fc.getSelectedFile(); 
 // Jika file belum ada, atau sudah ada 
 // tetapi OK untuk ditimpa -> SimpanFile 
 if (!file.exists() || okToReplace()) 
 SimpanFile(); 
 } 
    }//GEN-LAST:event_btnSaveAsActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        if (fileBaru) 
 //Jika fileBaru, Picu tombol Save As untuk event Klik; 
 btnSaveAs.doClick(); 
 else 
 // kalo bukan file baru, simpan langsung 
 SimpanFile(); 
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnOpenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOpenActionPerformed
        int hasil = fc.showOpenDialog(this); 
 if (hasil == JFileChooser.APPROVE_OPTION) { 
 //Pilih Yes
 file = fc.getSelectedFile(); 
 try { 
 editor.read(new FileInputStream(file), null); 
 } catch (IOException e) { 
 msg("Gagal Buka File : "+file.getName() ); 
 return ; 
 } 
 } 
 // Jika berhasil buka, munculkan nama file 
 // dan variabel fileBaru menjadi FALSE 
 this.setTitle(file.getName()+" - NOTES" ); 
 fileBaru = false; 
    }//GEN-LAST:event_btnOpenActionPerformed

    private void btnKeluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnKeluarActionPerformed
        System.exit(0); 
    }//GEN-LAST:event_btnKeluarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        new fNotes().setVisible(true);
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(fNotes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(fNotes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(fNotes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(fNotes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new fNotes().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnKeluar;
    private javax.swing.JButton btnNew;
    private javax.swing.JButton btnOpen;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnSaveAs;
    private javax.swing.JEditorPane editor;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
private void msg(String pesan) { 
 // untuk memunculkan pesan dengan tombol OK 
 JOptionPane.showMessageDialog(this,pesan,"Informasi", 
 JOptionPane.INFORMATION_MESSAGE); 
    }
    private boolean okToReplace() { 
 //Jika Pilihan mengembalikan 0 (Yes) berarti true 
 return (JOptionPane.showConfirmDialog(this, 
 "File "+file.getName()+" Sudah Ada\n"+ 
 "Akan Ditimpa ?", 
 "Peringatan",JOptionPane.YES_NO_OPTION)==0) ; 
 } 
    private void SimpanFile() { 
 PrintWriter pw = null; 
 try { 
 pw = new PrintWriter(new BufferedWriter(new FileWriter(file))); 
 } catch (IOException e) { 
 msg("Gagal Simpan File : '" + file.getName()); 
 return; 
 } 
 pw.print(editor.getText()); 
 pw.close(); 
 // Jika berhasil simpan, munculkan nama file 
 // dan variabel fileBaru menjadi FALSE 
 this.setTitle(file.getName()+" - NOTES" ); 
 fileBaru = false; 
 }
}
class jenisFile extends FileFilter { 
 private String[] s; 
 jenisFile(String[] sArg) { 
 s = sArg; 
 } 
 // Tentukan file apa yang akan ditampilkan 
 public boolean accept(File fArg) { 
 if (fArg.isDirectory()) 
 return true; 
 // Jika Extension dari file sesuai syarat, tampilkan 
for (int i = 0; i < s.length; ++i) 
 if (fArg.getName().toLowerCase().indexOf(s[i].toLowerCase()) > 0) 
 return true; 
 // selain kedua hal di atas, anggap false 
 return false; 
 } 
 public String getDescription() { 
 String tmp = ""; 
 for (int i = 0; i < s.length; ++i) 
 tmp += "*" + s[i] + " "; 
 return tmp; 
 } 
} 

