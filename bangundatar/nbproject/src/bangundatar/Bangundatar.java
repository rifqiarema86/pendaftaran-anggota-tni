package bangundatar;
public class Bangundatar {
    private double luas,keliling;
    final double phi = 3.14;
    
    public double getLuas(){
         return luas;
    }
    public double getKeliling(){
        return keliling;
    }
    public void setLuas(double newluas){
        this.luas = newluas;
    }
    public void setKeliling(double newkeliling){
        this.keliling = newkeliling;
    }
}
