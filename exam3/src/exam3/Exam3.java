/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package exam3;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class Exam3 {
    public static void main(String[] args) {
        // Contoh list buah-buahan
        String[] fruits = {"apel", "mangga", "Durian", "Mangga", "Apel", "MANGGA", "mangga"};
        Map<String, Integer> fruitCounts = countFruits(fruits);
        String result = sortAndFormatCounts(fruitCounts);
        System.out.println(result);
    }
    private static Map<String, Integer> countFruits(String[] fruits) {
        Map<String, Integer> fruitCounts = new HashMap<>();
        for (String fruit : fruits) {
            fruit = fruit.toLowerCase();
            fruitCounts.put(fruit, fruitCounts.getOrDefault(fruit, 0) + 1);
        }
        return fruitCounts;
    }
    private static String sortAndFormatCounts(Map<String, Integer> fruitCounts) {
        return fruitCounts.entrySet().stream()
                .sorted((entry1, entry2) -> {
                    int compare = entry2.getValue().compareTo(entry1.getValue());
                    return compare != 0 ? compare : entry1.getKey().compareTo(entry2.getKey());
                })
                .map(entry -> entry.getValue() > 1 ? entry.getKey() + " " + entry.getValue() : entry.getKey())
                .collect(Collectors.joining("\n"));
    }
}



