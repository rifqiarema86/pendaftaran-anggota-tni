package konsumsi;
public class makanan {
    private int karbohidrat;
    private String namaMakanan;
    
    public String getNamaMakanan() {
        return namaMakanan;
    }
    public void setNamaMakanan(String namaMakanan) {
        this.namaMakanan = namaMakanan;
    }
    public int getKarbohidrat() {
        return karbohidrat;
    }
    public void setKarbohidrat(int karbohidrat) {
        this.karbohidrat = karbohidrat;
    }
}
