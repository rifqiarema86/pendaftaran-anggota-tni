package konsumsi;
public class minuman {
    private int lemak;
    private String namaMinuman;
    
    public String getNamaMinuman() {
        return namaMinuman;
    }
    public void setNamaMinuman(String namaMinuman) {
        this.namaMinuman = namaMinuman;
    }
    public int getLemak() {
        return lemak;
    }
    public void setLemak(int lemak) {
        this.lemak = lemak;
    }
}
