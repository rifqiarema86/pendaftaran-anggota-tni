package konsumsi;
import java.util.ArrayList;
public class main {
 public static void main(String[] args){
 ArrayList<Konsumsi> listKonsumsi = new ArrayList<>();
 Konsumsi<makanan,minuman> breakfast = new Konsumsi<>();
 Konsumsi<makanan,minuman> lunch = new Konsumsi<>();
 makanan roti = new makanan();
 roti.setNamaMakanan("Roti Tawar");
 roti.setKarbohidrat(15);
 minuman susu = new minuman();
 susu.setLemak(9);
 susu.setNamaMinuman("Susu Sapi");
 breakfast.setKonsumsi(roti,susu);
 listKonsumsi.add(breakfast);
 makanan nasi = new makanan();
 nasi.setNamaMakanan("Nasi Putih");
 nasi.setKarbohidrat(50);
 minuman air = new minuman();
 air.setLemak(0);
 air.setNamaMinuman("Air Putih");
 lunch.setKonsumsi(nasi,air);
 listKonsumsi.add(lunch);
 System.out.println("Menu Konsumsi");
 for (Konsumsi<makanan,minuman> konsumsi: listKonsumsi ) {
 makanan makanan = konsumsi.getM();
 minuman minuman = konsumsi.getI();
 System.out.println("Makanan : "+makanan.getNamaMakanan());
 System.out.println("Karbohidrat : "+makanan.getKarbohidrat());
 System.out.println("Minuman : "+minuman.getNamaMinuman());
 System.out.println("Lemak : "+minuman.getLemak());
 }
 }
}

