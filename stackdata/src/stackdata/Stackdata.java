package stackdata;
import java.util.*;
public class Stackdata {
    public static void main(String[] args) {
         Scanner sc = new Scanner(System.in);
         System.out.println("Enter the word: ");
         String inputString = sc.nextLine();
         String reversedString = reverseString(inputString);
         System.out.println("Reversed: " + reversedString);
    }
    public static String reverseString(String inputString) {
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < inputString.length(); i ++){
            char ch = inputString.charAt(i);
            stack.push(ch);
    }
        String reversedString = "";
        while (!stack.empty()) {
            reversedString += stack.pop();
        }
        return reversedString;
        
        
    }
    
}
