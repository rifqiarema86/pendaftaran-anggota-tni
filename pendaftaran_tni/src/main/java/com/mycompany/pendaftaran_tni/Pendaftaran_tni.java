package com.mycompany.pendaftaran_tni;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Pendaftaran_tni {

    public static void main(String[] args) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            // Mengatur koneksi ke PostgreSQL
            String url = "jdbc:postgresql://localhost:5432/Pendaftaran";
            String user = "postgres";
            String password = "181207";

            // Membuat koneksi
            connection = DriverManager.getConnection(url, user, password);

            if (connection != null) {
                System.out.println("Koneksi ke PostgreSQL berhasil!");

                // Membuat pernyataan SQL untuk mengambil data
                String sql = "SELECT * FROM \"tb_User\"";
                preparedStatement = connection.prepareStatement(sql);

                // Mengeksekusi pernyataan SQL
                resultSet = preparedStatement.executeQuery();

                // Mendapatkan metadata untuk mengambil nama kolom secara case-insensitive
                java.sql.ResultSetMetaData metaData = resultSet.getMetaData();
                int columnCount = metaData.getColumnCount();

                // Menampilkan hasil query
                while (resultSet.next()) {
                    for (int i = 1; i <= columnCount; i++) {
                        String columnName = metaData.getColumnName(i);
                        String columnValue = resultSet.getString(i);

                        System.out.println(columnName + ": " + columnValue);
                    }
                }

            } else {
                System.out.println("Gagal melakukan koneksi ke PostgreSQL.");
            }

        } catch (SQLException e) {
            System.out.println("Terjadi kesalahan saat mencoba terhubung ke PostgreSQL atau menjalankan kueri.");
            e.printStackTrace();
        } finally {
            // Menutup sumber daya setelah selesai
            try {
                if (resultSet != null) resultSet.close();
                if (preparedStatement != null) preparedStatement.close();
                if (connection != null) connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
