package queue;
 public class Queue {
    private Node front;
    private Node rear;
    private int size;
 
    public Queue() {
        front = null;
        rear = null;
        size = 0;
    }
 
    private class Node {
        int data;
        Node next;
 
        public Node(int data) {
            this.data = data;
            this.next = null;
        }
    }
 
    public void enqueue(int data) {
        Node newNode = new Node(data);
        if (isEmpty()) {
            front = newNode;
            rear = newNode;
        } else {
            rear.next = newNode;
            rear = newNode;
        }
        size++;
    }
 
    public int dequeue() {
        if (isEmpty()) {
            System.out.println("Queue is empty");
            return -1;
        } else {
            int data = front.data;
            front = front.next;
            size--;
            return data;
        }
    }
 
    public int peek() {
        if (isEmpty()) {
            System.out.println("Queue is empty");
            return -1;
        } else {
            return front.data;
        }
    }
 
    public boolean isEmpty() {
        return (size == 0);
    }
 
    public int size() {
        return size;
    }
 
    public static void main(String[] args) {
        Queue queue = new Queue();
        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);
 
        System.out.println("Dequeued item: " + queue.dequeue());
 
        System.out.println("Front item: " + queue.peek());
 
        System.out.println("Queue size: " + queue.size());
 
        System.out.println("Is queue empty? " + queue.isEmpty());
    }
 }

