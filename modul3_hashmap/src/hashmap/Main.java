/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package hashmap;

/**
 *
 * @author rudiyanto
 */
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
        // contoh data akun
        HashMap<String, String> tabelAkun = new HashMap<String, String>();
        tabelAkun.put("user1@example.com", "password1");
        tabelAkun.put("user2@example.com", "password2");
        tabelAkun.put("user3@example.com", "password3");
        
        // membuat objek tabelSesiLogin dari objek tabelAkun
        TabelSesiLogin tabelSesiLogin = new TabelSesiLogin(tabelAkun);
        // melakukan login akun
        tabelSesiLogin.loginAkun("user1@example.com", "password1"); 
        tabelSesiLogin.loginAkun("user2@example.com", "password2");
            
        // melakukan logout akun
        tabelSesiLogin.logoutAkun("user1@example.com"); // true
        
        // melihat total logout, total login, dan total auth
        System.out.println("Total logout: " + tabelSesiLogin.totalLogout()); // 1
        System.out.println("Total login: " + tabelSesiLogin.totalLogin()); // 2
        System.out.println("Total auth: " + tabelSesiLogin.totalAuth()); // 1
    }
}
