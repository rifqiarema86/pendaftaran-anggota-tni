/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hashmap;

/**
 *
 * @author rudiyanto
 */
import java.util.HashMap;

public class TabelSesiLogin {
    private HashMap<String, String> tabelSesiLogin = new HashMap<String, String>();
    private HashMap<String, String> tabelAkun;
    private int totalLogout = 0;
    private int totalLogin = 0;
    
    public TabelSesiLogin(HashMap<String, String> tabelAkun) {
        this.tabelAkun = tabelAkun;
    }

    public boolean loginAkun(String email, String password) {
        if (tabelAkun.containsKey(email) && tabelAkun.get(email).equals(password)) {
            tabelSesiLogin.put(email, password);
            totalLogin++;
            return true;
        }
        return false;
    }

    public boolean logoutAkun(String email) {
        if (tabelSesiLogin.containsKey(email)) {
            tabelSesiLogin.remove(email);
            totalLogout++;
            return true;
        }
        return false;
    }

    public int totalLogout() {
        return totalLogout;
    }

    public int totalLogin() {
        return totalLogin;
    }

    public int totalAuth() {
        return tabelSesiLogin.size();
    }
}

