package mahasiswa;
import java.util.ArrayList;

public class main {
  public static void main(String[] args) {
    ArrayList<String> namaMahasiswa = new ArrayList<String>();
    namaMahasiswa.add("Lala");
    namaMahasiswa.add("Shani");
    namaMahasiswa.add("Gracia");
    namaMahasiswa.add("Kyla");
    namaMahasiswa.add("Gio");
    
    // Menyisipkan elemen baru pada posisi tertentu
    namaMahasiswa.add(2, "Jamal");
    namaMahasiswa.add(3, "Ageng");
    
    namaMahasiswa.remove(2);
    namaMahasiswa.remove(3);
    
     int posisiLala = namaMahasiswa.indexOf("Lala");
    int posisiGracia = namaMahasiswa.indexOf("Gracia");

    
    // Mencetak semua elemen pada ArrayList namaMahasiswa
    System.out.println("Setelah menyisipkan elemen pada posisi tertentu:");
    for (String nama : namaMahasiswa) {
      System.out.println(nama);
    }
    // Mencetak semua elemen pada ArrayList namaMahasiswa
    System.out.println("Setelah menghapus elemen pada posisi tertentu:");
    for (String nama : namaMahasiswa) {
      System.out.println(nama);
    }
   
    System.out.println("Indeks Lala: " + posisiLala);
    System.out.println("Indeks Gracia: " + posisiGracia);
  }
}
