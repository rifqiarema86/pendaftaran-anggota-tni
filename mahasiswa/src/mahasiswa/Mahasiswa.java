package mahasiswa;
import java.util.ArrayList;

public class Mahasiswa {
    public static void main(String[] args) {
        ArrayList<String> namaMahasiswa = new ArrayList<>();
        namaMahasiswa.add("Lala"); // Index 0
        namaMahasiswa.add("Shani"); // Index 1
        namaMahasiswa.add("Gracia"); // Index 2
        namaMahasiswa.add("Kyla"); // Index 3
        namaMahasiswa.add("Gio"); // Index 4

        // Menyisipkan elemen baru
        namaMahasiswa.add(0, "Jamal"); // Index 0
        namaMahasiswa.add(3, "Ageng"); // Index 3

        // Menghapus elemen pada posisi tertentu
        namaMahasiswa.remove(2); // Hapus pada posisi ke-2 (Shani)
        namaMahasiswa.remove(3); // Hapus pada posisi ke-4 (Kyla)
        
        namaMahasiswa.add(3, "Gracia");

        // Mencari posisi (index) Lala dan Gracia
        int posisiLala = namaMahasiswa.indexOf("Lala");
        int posisiGracia = namaMahasiswa.indexOf("Gracia");

        // Menampilkan isi ArrayList dan posisi Lala dan Gracia
        System.out.println("Isi ArrayList namaMahasiswa:");
        for (int i = 0; i < namaMahasiswa.size(); i++) {
            System.out.println("Index " + i + " = " + namaMahasiswa.get(i));
        }
        System.out.println("Posisi Lala = " + posisiLala);
        System.out.println("Posisi Gracia = " + posisiGracia);
    }
}
