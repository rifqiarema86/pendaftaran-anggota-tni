/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package animal;

/**
 *
 * @author rudiyanto
 */
public class sapi extends animal {
    private boolean jeniskelamin;
    
    public boolean getsapi(boolean jk){
        jeniskelamin=jk;
        if(jk==true){
            System.out.println("Jenis kelamin : Betina");
        }else{
            System.out.println("Jenis kelamin : Jantan");
        }
        return jeniskelamin;
    }
    
    public void setsapi (boolean jk){
        this.jeniskelamin=jk;
    }
    public void reproduksi(){
        System.out.println("Reproduksi : Melahirkan");
    }
    public void bersuara(){
        System.out.println("Suara Sapi : MO MO MO");
    }
    public void makan(){
        System.out.println("Makanan : Rumput\n");
    }
    
    void cetakdata(String jenis,int jumlahkaki){
        super.cetakdata(jenis,jumlahkaki);
        System.out.println("Jenis : "+jenis);
        System.out.println("Jumlah kaki : "+jumlahkaki);
    }
}
