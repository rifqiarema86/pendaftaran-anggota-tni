package animal;

/**
 *
 * @author rudiyanto
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        sapi s = new sapi();
        s.cetakdata("Mamalia", 4);
        s.getsapi(true);
        s.bersuara();
        s.reproduksi();
        s.makan();
        
        Ayam a = new Ayam();
        a.cetakdata("Unggas", 2);
        a.getayam(true);
        a.bersuara();
        a.reproduksi();
        a.makan();
        
        anak_ayam aa = new anak_ayam();
        aa.cetakdata("Unggas", 2);
        aa.getayam(true);
        aa.bersuara();
        aa.reproduksi();
        aa.makan();
    }
    
}
