/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package exam4;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
public class Exam4 {
    private static Map<String, Double> exchangeRates = new HashMap<>();
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            String input = scanner.nextLine();
            String[] tokens = input.split("\\s+");

            if (tokens[0].equalsIgnoreCase("KURS")) {
                // Perintah untuk mengganti nilai kurs mata uang
                setExchangeRate(tokens[1], Double.parseDouble(tokens[2]));
            } else if (tokens[0].equalsIgnoreCase("CONV")) {
                // Perintah untuk konversi mata uang
                convertCurrency(tokens[1], Double.parseDouble(tokens[2]));
            } else {
                System.out.println("Perintah tidak valid.");
            }
        }
    }
    private static void setExchangeRate(String currencyCode, double rate) {
        exchangeRates.put(currencyCode, rate);
    }
    private static void convertCurrency(String currencyCode, double amount) {
        if (exchangeRates.containsKey(currencyCode)) {
            double exchangeRate = exchangeRates.get(currencyCode);
            double result = amount / exchangeRate;
            System.out.printf("USD %.2f%n", result);
        } else {
            System.out.println(currencyCode + " -> unknown");
        }
    }
}

