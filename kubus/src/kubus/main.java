package kubus;
public class main {
    public static void main(String[] args) {
        Kubus<Double> kubusTipeDouble = new Kubus<>(1.5, 2.0, 3.0);
        System.out.println(kubusTipeDouble.toString());
        System.out.println("Volume: " + kubusTipeDouble.getResultAsDouble());

        Kubus<Integer> kubusTipeInteger = new Kubus<>(2, 3, 4);
        System.out.println(kubusTipeInteger.toString());
        System.out.println("Volume: " + kubusTipeInteger.getResultAsInt());
    }
}
   