package kubus;
public class Kubus<T extends Number> {
    private T panjang;
    private T lebar;
    private T tinggi;

    public Kubus(T panjang, T lebar, T tinggi) {
        this.panjang =   panjang;
        this.lebar = lebar;
        this.tinggi = tinggi;
    }

    public String toString() {
        return "Panjang: " + panjang + ", Lebar: " + lebar + ", Tinggi: " + tinggi;
    }

    public long getResultAsLong() {
        return panjang.longValue() * lebar.longValue() * tinggi.longValue();
    }

    public int getResultAsInt() {
        return panjang.intValue() * lebar.intValue() * tinggi.intValue();
    }

    public double getResultAsDouble() {
        return panjang.doubleValue() * lebar.doubleValue() * tinggi.doubleValue();
    }
}
