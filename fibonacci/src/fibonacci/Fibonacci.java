
package fibonacci;
 import java.util.Scanner;

public class Fibonacci {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Masukkan dataset kalimat: ");
        String sentence = scanner.nextLine();
        
        // Mengubah kalimat menjadi array of string
        String[] words = sentence.split(" ");
        
        // Melakukan sorting pada array
        sort(words);
        
        // Meminta user untuk memasukkan kata yang ingin dicari
        System.out.print("Masukkan kata yang ingin dicari: ");
        String target = scanner.nextLine();
        
        // Mencari kata menggunakan algoritma pencarian Fibonacci
        int result = fibonacciSearch(words, target);
        
        // Output hasil pencarian
        if (result == -1) {
            System.out.println("Kata \"" + target + "\" tidak ditemukan dalam kalimat");
        } else {
            System.out.println("Kata \"" + target + "\" ditemukan pada index ke " + result);
        }
    }
    
    // Algoritma pencarian Fibonacci untuk mencari kata dalam array of string
    public static int fibonacciSearch(String[] arr, String target) {
        int n = arr.length;
        int fib1 = 0;
        int fib2 = 1;
        int fib3 = fib1 + fib2;
        
        // Mencari bilangan Fibonacci terbesar yang <= n
        while (fib3 < n) {
            fib1 = fib2;
            fib2 = fib3;
            fib3 = fib1 + fib2;
        }
        
        // Variabel untuk menyimpan indeks terakhir
        int last = -1;
        
        while (fib3 > 1) {
            int i = Math.min(last + fib1, n - 1);
            
            // Jika target lebih besar dari elemen di indeks i
            if (arr[i].compareTo(target) < 0) {
                fib3 = fib2;
                fib2 = fib1;
                fib1 = fib3 - fib2;
                last = i;
            }
            // Jika target lebih kecil dari elemen di indeks i
            else if (arr[i].compareTo(target) > 0) {
                fib3 = fib1;
                fib2 = fib2 - fib1;
                fib1 = fib3 - fib2;
            }
            // Jika target sama dengan elemen di indeks i
            else {
                return i;
            }
        }
        
        // Jika elemen tidak ditemukan
        if (fib2 == 1 && arr[last + 1].compareTo(target) == 0) {
            return last + 1;
        }
        
        return -1;
    }
    
    // Algoritma sorting untuk array of string
    public static void sort(String[] arr) {
        int n = arr.length;
        int fib1 = 0;
        int fib2 = 1;
        int fib3 = fib1 + fib2;
        
        // Mencari bilangan Fibonacci terbesar yang <= n
        while (fib3 < n) {
            fib1 = fib2;
            fib2 = fib3;
            fib3 = fib1 + fib2;
        }
        
        int i = -1;
        while (fib3 > 1) {
            int j = i + fib1;
            if (j < n - 1 && arr[j].compareTo(arr[j+1]) > 0) {
                swap(arr, j, j+1);
            }
                       if (j > 0 && arr[j-1].compareTo(arr[j]) > 0) {
                swap(arr, j-1, j);
            }
            
            i = i + fib1;
            fib3 = fib2;
            fib2 = fib1;
            fib1 = fib3 - fib2;
        }
        
        // Melakukan bubble sort pada elemen terakhir jika belum terurut
        if (fib2 == 1 && i < n - 1 && arr[i].compareTo(arr[i+1]) > 0) {
            swap(arr, i, i+1);
        }
    }
    
    // Fungsi untuk melakukan swap pada array of string
    public static void swap(String[] arr, int i, int j) {
        String temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
}

